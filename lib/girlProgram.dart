import 'dart:io';

girlProgram2() {

  int heightDesired;
  int weightDesired;
  String hairColorDesired;

  int heightActual;
  int weightActual;
  String hairColorActual;

  print("Hello, let's check if your girlfriend fits you \n ... \n ...");

  stdout.writeln("What is your desired girl's height in cm? (ex. - 170])");
  heightDesired = int.parse(stdin.readLineSync());

  stdout.writeln("How much should she weight in kg? (ex. 50)");
  weightDesired = int.parse(stdin.readLineSync());

  stdout.writeln("What hair color would you like her to have?");
  hairColorDesired = stdin.readLineSync();

  print(
      "Ok, let's now learn the parameters of a girl that you have met. \n ... \n ...");

  stdout.writeln("What is the current girl's height in cm? (ex. 160)");
  heightActual = int.parse(stdin.readLineSync());

  stdout.writeln("What is her weight in kg? (ex. 70)");
  weightActual = int.parse(stdin.readLineSync());

  stdout.writeln("What is her hair color?");
  hairColorActual = stdin.readLineSync();

  (heightActual >= heightDesired && heightActual <= 178
      && weightActual <= weightDesired && weightDesired < 58
      && hairColorActual == hairColorDesired)
      ? print("This girl fits you, she is georgeous!")
      : print(
      "She is not good enough, you can find a way better! \n - - \n - -");

  print("Desired: \n $heightDesired $weightDesired $hairColorDesired");
  print("Actual: \n $heightActual $weightActual $hairColorActual");
  print("\n (''')O_o(''') : by Shakle :P \n \n");

  stdout.writeln("Press enter to close the program...");
}